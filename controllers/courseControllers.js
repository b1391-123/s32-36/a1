const Course = require("../models/Course");

module.exports.createCourse = (reqBody) => {
    let newCourse = new Course({
        courseName: reqBody.courseName,
        description: reqBody.description,
        price: reqBody.price,
    });
    return newCourse.save().then((result, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    });
};

module.exports.getAllCourses = () => {
    return Course.find().then((result, error) => {
        if (error) {
            return false;
        } else {
            return result;
        }
    });
};

module.exports.getActiveCourses = () => {
    return Course.find({ isActive: true }).then((result, error) => {
        if (error) {
            return false;
        } else {
            return result;
        }
    });
};

module.exports.getSpecificCourse = (name) => {
    return Course.findOne({ courseName: name }).then((result, error) => {
        if (error) {
            return false;
        } else {
            return result;
        }
    });
};

module.exports.getById = (id) => {
    return Course.findById({ _id: id }).then((result, error) => {
        if (result == null) {
            return `Course not existing`;
        } else {
            if (error) {
                return false;
            } else {
                return result;
            }
        }
    });
};

module.exports.archieveCourse = (name) => {
    let updatedIsActive = {
        isActive: false,
    };
    return Course.findOneAndUpdate({ courseName: name }, updatedIsActive).then(
        (result) => {
            if (result == null) {
                return `Course not existing`;
            } else {
                if (!result) {
                    return false;
                } else {
                    return result;
                }
            }
        }
    );
};

module.exports.unarchieveCourse = (name) => {
    let updatedIsActive = {
        isActive: true,
    };
    return Course.findOneAndUpdate({ courseName: name }, updatedIsActive).then(
        (result) => {
            if (result == null) {
                return `Course not existing`;
            } else {
                if (!result) {
                    return false;
                } else {
                    return result;
                }
            }
        }
    );
};

module.exports.archieveCourseById = (id) => {
    let updatedIsActive = {
        isActive: false,
    };
    return Course.findByIdAndUpdate({ _id: id }, updatedIsActive).then(
        (result) => {
            if (result == null) {
                return `Course not existing`;
            } else {
                if (!result) {
                    return false;
                } else {
                    return result;
                }
            }
        }
    );
};

module.exports.unArchieveCourseById = (id) => {
    let updatedIsActive = {
        isActive: true,
    };
    return Course.findByIdAndUpdate({ _id: id }, updatedIsActive).then(
        (result) => {
            if (result == null) {
                return `Course not existing`;
            } else {
                if (!result) {
                    return false;
                } else {
                    return result;
                }
            }
        }
    );
};

module.exports.deleteCourse = (name) => {
    return Course.findOneAndDelete({ courseName: name }).then((result) => {
        (result) => {
            if (result == null) {
                return `Course not existing`;
            } else {
                if (!result) {
                    return false;
                } else {
                    return result;
                }
            }
        };
    });
};

module.exports.deleteCourseById = (id) => {
    return Course.findByIdAndDelete({ _id: id }).then((result) => {
        (result) => {
            if (result == null) {
                return `Course not existing`;
            } else {
                if (!result) {
                    return false;
                } else {
                    return result;
                }
            }
        };
    });
};
